# Maproxy

Proxy for google maps requests. Use this if you have problems with CORS connection.

## Requirements

In order to start run/developing the application it is necessary to have `node.js` and `npm` installed.

```
node -v
>>> v8.11.3
```
```
npm -v
>>> 6.5.0
```

## Instalation

Clone the repository

```
git@gitlab.com:vysoki/maproxy.git
```

Install all packages

```
npm install
```

## Developer mode

```
npm dev
```