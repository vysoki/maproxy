const axios = require('axios');

module.exports = function(app, db) {
    app.get('/googleroute/:route', (req, res) => {
        axios.get('https://maps.googleapis.com/maps/api/directions/json?' + req.params.route)
        .then(function (response) {
            if (response.data.status == "OK") {
                res.send(response.data);
            }
            else {
                res.status(400).send("Error");
            }
        })
        .catch(function (error) {
            res.status(500).send("There was an 500 error!");
        });
    })
}